package com.example.week2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.Collection;

import static com.example.week2.App.CHANNEL_ID;


public class ExampleService extends Service implements BeaconConsumer{

    protected static final String TAG = "MonitoringService";
    private BeaconManager beaconManager;
    private float d;
    private String input;
    private final MemoryPersistence persistence = new MemoryPersistence();
    private MqttAndroidClient mqttAndroidClientt;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate() {
        super.onCreate();

        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        beaconManager.bind(this);


        mqttAndroidClientt = new MqttAndroidClient(this.getApplicationContext(), "tcp://88.99.124.84:1883", "androidSampleClient", persistence);
        mqttAndroidClientt.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                System.out.println("Connection was lost!");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                Log.d(TAG, "Message Arrived!: " + topic + ": " + new String(message.getPayload()));
                String msg = new String(message.getPayload());
                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(ExampleService.this)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle("Горит!")
                                .setContentText("Температура - " + String.valueOf(msg) + ". Бегите домой и тушите все!!");

                Notification notificationn = builder.build();

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(2, notificationn);
                //  textView.setText("Сообщение доставлено");
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                System.out.println("Delivery Complete!");
            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(true);

        try {
            mqttAndroidClientt.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    System.out.println("Connection Success!");
                    Log.d(TAG,"Connection Success!");
                    try {
                        System.out.println("Subscribing to /jopagorit");
                        mqttAndroidClientt.subscribe("jopagorit", 0);
                    } catch (MqttException ex) {
                        System.out.println(ex.toString());
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    System.out.println("Connection Failure!");
                    System.out.println("throwable: " + exception.toString());
                }
            });
        } catch (MqttException ex) {
            System.out.println(ex.toString());
        }


    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier(new RangeNotifier() {

            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                if (beacons.size() > 0) {
                    d = (float) beacons.iterator().next().getDistance();
                    input = String.format("Расстояние до beacon - %s метров", String.valueOf(d));
                }
                Intent serviceIntent = new Intent(getApplicationContext(), ExampleService.class);
                String p = String.valueOf(d);
                serviceIntent.putExtra("dExtra", p);
                serviceIntent.putExtra("inputExtra", input);
                if (p != null) {
                    Log.d(TAG, p);
                }
                ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);
            }
        });

        int majorId = 1791;
        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", Identifier.parse("00F0DBE5-688A-4EF0-BEDC-DEAC3E00A9DB"), Identifier.parse("1791"), null));
        } catch (RemoteException e) {    }
    }





    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onBeaconServiceConnect();
        String rangeValue = intent.getStringExtra("dExtra");
        String input = intent.getStringExtra("inputExtra");
        if (rangeValue != null) {
            Log.d(TAG, "onStartCommand " + rangeValue);
        }
        Intent notificationIntent = new Intent(BeaconActivity.BROADCAST_ACTION);
        notificationIntent.putExtra("rangeValue", rangeValue);
        sendBroadcast(notificationIntent);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Поиск маячка Beacon")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_beacon)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

//        String msg = intent.getStringExtra("msg");
//        Log.d(TAG, "К нам пришло " + msg);



        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

