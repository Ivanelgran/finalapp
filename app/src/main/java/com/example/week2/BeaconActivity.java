package com.example.week2;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class BeaconActivity extends Activity {

    public final static String BROADCAST_ACTION = "com.example.week2";
    BroadcastReceiver br;
    protected static final String TAG = "MonitoringActivity";
    private TextView textView;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon);
        imageView = (ImageView) findViewById(R.id.imageView4);

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "Получено сообщение");
                String rangeValue = intent.getStringExtra("rangeValue");
                if (rangeValue != null) {
                    Log.d(TAG, "Beacon-activity " + rangeValue);
                    float value = Float.parseFloat(rangeValue);

                    textView = findViewById(R.id.textView2);
                    TextView textView2 = findViewById(R.id.textView3);
                    if (value < 3) {
                        String s = String.format("Расстояние до beacon - %s метров", String.valueOf(value));
                        textView.setText(String.valueOf(s));
                        textView2.setText("Уровень сигнала:");
                        imageView.setImageResource(R.drawable.good);
                    } else if (3 < value && value < 5) {
                        String s = String.format("Расстояние до beacon - %s метров", String.valueOf(value));
                        textView.setText(String.valueOf(s));
                        textView2.setText("Уровень сигнала:");
                        imageView.setImageResource(R.drawable.mid);
                    } else {
                        String s = String.format("Расстояние до beacon - %s метров", String.valueOf(value));
                        textView.setText(String.valueOf(s));
                        textView2.setText("Уровень сигнала:");
                        imageView.setImageResource(R.drawable.bad);
                    }
                    Log.i(TAG, "Наш beacon сейчас на расстоянии около " + String.valueOf(value) + " метров.");
                }
            }
        };
        IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        // регистрируем (включаем) BroadcastReceiver
        registerReceiver(br, intFilt);
//        String rangeValue = "1";
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    boolean valera = true;

    public void stopService(View v) {
        if (valera == true) {
            Intent serviceIntent = new Intent(getApplicationContext(), ExampleService.class);
            ContextCompat.startForegroundService(getApplicationContext(), serviceIntent);
            valera = false;
        } else {
            Intent serviceIntent = new Intent(this, ExampleService.class);
            stopService(serviceIntent);
            valera = true;
        }
    }
}
