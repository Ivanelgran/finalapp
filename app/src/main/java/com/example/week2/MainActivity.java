package com.example.week2;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import static com.example.week2.App.CHANNEL_ID;


public class MainActivity extends AppCompatActivity implements OnClickListener {


    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    ImageView imageView;
    Button btnBeacon;
    Button btnActRecord;
    Button btnTTS;
    Button btnSensors;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        imageView = (ImageView) findViewById(R.id.imageView);
        btnBeacon = (Button) findViewById(R.id.btnBeacon);
        btnBeacon.setOnClickListener(this);
        btnActRecord = (Button) findViewById(R.id.btnActRecord);
        btnActRecord.setOnClickListener(this);
        btnTTS = (Button) findViewById(R.id.btnTTS);
        btnTTS.setOnClickListener(this);
        btnSensors = (Button) findViewById(R.id.btnSensors);
        btnSensors.setOnClickListener(this);
        Log.d(TAG, "Успешная инициализация main окна");
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Доступ к местоположению получен");

                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Запрос местоположения");
                    builder.setMessage("Без доступа к текущему местоположению приложение не сможет производить поиск beacon в фоне");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            requestPermissions(new String[]{
                                            Manifest.permission.ACCESS_COARSE_LOCATION,
                                            Manifest.permission.BLUETOOTH_ADMIN,
                                            Manifest.permission.RECORD_AUDIO,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PERMISSION_REQUEST_COARSE_LOCATION);
                        }
                    });
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBeacon:
                Log.d(TAG, "Нажата кнопка соединения с Beacon");
                Intent intentssss = new Intent(this, BeaconActivity.class);
                startActivity(intentssss);
                break;
            case R.id.btnActRecord:
                Log.d(TAG, "Нажата кнопка с записью аудио");
                Intent intentRecord = new Intent(this, NewRecordActivity.class);
                startActivity(intentRecord);
                break;
            case R.id.btnTTS:
                Log.d(TAG, "Нажата кнопка с синтезатором речи");
                Intent intentsssss = new Intent(this, TestTTSActivity.class);
                startActivity(intentsssss);
                break;
            case R.id.btnSensors:
                Log.d(TAG, "Нажата кнопка с показаниями датчиков");
                Intent intentSensors = new Intent(this, SensorsActivity.class);
                startActivity(intentSensors);
                break;
            default:
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "Пользователь перестал взаимодействовать с main окном");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "Пользователь вышел из main окна");
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "Пользователь вернулся в mail окно");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Пользователь просматривает main окно");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Пользователь закрыл приложение");
    }
}