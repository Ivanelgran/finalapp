package com.example.week2;

import android.content.Intent;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class SensorsActivity extends AppCompatActivity {

    protected static final String TAG = "SensorsActivity";
    private final MemoryPersistence persistence = new MemoryPersistence();
    private MqttAndroidClient mqttAndroidClient;
    ListView listView;
    Button btnVoice;
    String temperature;
    String humidity;

    private TextToSpeech mTTS;
    // авторизация и отправление в FireBase storage
    private String fileName;
    public String url;
    private StorageReference mStorageRef;
    private FirebaseAuth mAuth;
    private FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);

        listView = (ListView) findViewById(R.id.listView);
        btnVoice = (Button) findViewById(R.id.btnVoice);

        // Начальный listView
        ArrayList<String> data = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.custom_list_view, data);
        listView.setAdapter(adapter);
        data.add("Температура: ?");
        data.add("Влажность: ?");
        adapter.notifyDataSetChanged();

        // авторизация
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword("primer@mail.ru", "1234567")
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            user = mAuth.getCurrentUser();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                        }
                    }
                });

        mTTS = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTTS.setLanguage(new Locale("ru"));

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "Language not supported");
                    }
                } else {
                    Log.e("TTS", "Initialization failed");
                }
            }
        });

        mqttAndroidClient = new MqttAndroidClient(this.getApplicationContext(), "tcp://88.99.124.84:1883", "androidSampleClient", persistence);
        mqttAndroidClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                System.out.println("Connection was lost!");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                System.out.println("Message Arrived!: " + topic + ": " + new String(message.getPayload()));
                String msg = new String(message.getPayload());
                JSONObject jsonObject = new JSONObject(msg);

                // Обновляю данные
                temperature = jsonObject.getString("t");
                humidity = jsonObject.getString("h");
                data.clear();
                data.add("Температура: " + temperature);
                data.add("Влажность: " + humidity);
                adapter.notifyDataSetChanged();
                Toast.makeText(SensorsActivity.this, "Получены данные", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                System.out.println("Delivery Complete!");
            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(true);

        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    System.out.println("Connection Success!");
                    try {
                        System.out.println("Subscribing to /sensors");
                        mqttAndroidClient.unsubscribe("jopagorit");
                        mqttAndroidClient.subscribe("sensors", 0);
                        mqttAndroidClient.publish("getStatus", new MqttMessage("".getBytes()));
//                        while(true){
//                            try {
//                                Thread.sleep(2000);
//                                mqttAndroidClient.publish("getStatus", new MqttMessage("".getBytes()));
//                                Log.d(TAG, "Получаем статус");
//                            } catch (InterruptedException ex) {
//                                System.out.println(ex.toString());
//                                break;
//                            }
//                        }

                    } catch (MqttException ex) {
                        System.out.println(ex.toString());
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    System.out.println("Connection Failure!");
                    System.out.println("throwable: " + exception.toString());
                }
            });
        } catch (MqttException ex) {
            System.out.println(ex.toString());
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mqttAndroidClient.disconnect();
            Log.d(TAG, "Закрыли MQTT соединение");
            if (mTTS != null) {
                mTTS.stop();
                mTTS.shutdown();
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void voiceSensors(View v) {
        if (temperature!=null && humidity!=null){
            String message = "Текущая температура " + temperature + " градусов. Влажность " + humidity + " процентов";
            Log.d(TAG, message);
            speak(message);
        }
    }

    private void speak(String text) {
        File wavFile = new File(getFilesDir(), "record.wav");
        fileName = wavFile.getAbsolutePath();

        HashMap<String,String> myHashRender = new HashMap<>();
        myHashRender.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, text);

        int result = mTTS.synthesizeToFile(text, myHashRender, fileName);
        if (result == TextToSpeech.SUCCESS) Log.d("TTS", "файл сохранен");
        uploadRecord();
    }


    private void uploadRecord() {
        if (user != null) {
            if (fileName != null) {
                Uri file = Uri.fromFile(new File(fileName));
                StorageReference recordsRef = mStorageRef.child("records/record.wav");
                UploadTask uploadTask = recordsRef.putFile(file);

                // Register observers to listen for when the download is done or if it fails
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                        // ...
                    }
                });


                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return recordsRef.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();
                            url = downloadUri.toString();
                            Log.d(TAG, url);
                            String newUrl = url.substring(112);
                            Log.d(TAG, newUrl);
                            System.out.println(downloadUri.toString());

                            Intent intentMQTT = new Intent(getApplicationContext(), MqttActivity.class);
                            intentMQTT.putExtra("url", newUrl);
                            startActivity(intentMQTT);
                        } else {
                            // Handle failures
                            // ...
                        }
                    }
                });
            }
        }
    }
}
