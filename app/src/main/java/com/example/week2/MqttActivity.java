package com.example.week2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttActivity extends AppCompatActivity {

    protected static final String TAG = "MqttActivity";
    private final MemoryPersistence persistence = new MemoryPersistence();
    private MqttAndroidClient mqttAndroidClient;
    Button btnCool;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mqtt);
        btnCool=(Button) findViewById(R.id.buttonCool);
        btnCool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.buttonCool:
                        Intent intent = new Intent(MqttActivity.this, MainActivity.class);
                        startActivity(intent);
                        break;
                    default:
                        break;
                }
            }
        });

        String url = getIntent().getExtras().getString("url");
        Log.d(TAG, "Передали ссылку - " + url);




        mqttAndroidClient = new MqttAndroidClient(this.getApplicationContext(), "tcp://88.99.124.84:1883", "androidSampleClient", persistence);
        mqttAndroidClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                System.out.println("Connection was lost!");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                System.out.println("Message Arrived!: " + topic + ": " + new String(message.getPayload()));
              //  textView.setText("Сообщение доставлено");
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                System.out.println("Delivery Complete!");
            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(true);

        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    System.out.println("Connection Success!");
                    try {
                        System.out.println("Subscribing to /url");
                        mqttAndroidClient.subscribe("url", 0);
                    } catch (MqttException ex) {
                        System.out.println(ex.toString());
                    }

                    try {
//                        System.out.println(url);
                        mqttAndroidClient.publish("url", new MqttMessage(url.getBytes()));
                        Log.d(TAG, "Послали ссылку - " + url);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    System.out.println("Connection Failure!");
                    System.out.println("throwable: " + exception.toString());
                }
            });
        } catch (MqttException ex) {
            System.out.println(ex.toString());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            mqttAndroidClient.disconnect();
            Log.d(TAG, "Закрыли соединение");
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mqttAndroidClient.disconnect();
            Log.d(TAG, "Закрыли соединение");
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }
}